# `yt-rss.py`

Get YouTube RSS feed URL for a given channel.

```bash
yt-rss.py @OfficialABBA
```
