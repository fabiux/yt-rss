#!/usr/bin/env python
from sys import argv
import requests

if len(argv) < 2:
    exit('Usage: yt-rss.py <CHANNEL_NAME>')

r = requests.get('https://www.youtube.com/' + argv[1])
print(r.text.split('RSS')[1].split('"')[2])
